# x11-to-monochrome-fb

This is a hackish approach to get rid of implementing a fbdev supporting drm.

This is not meant to be used for production code at all. It is just a personal project
to fullfil the dream to paraglide with opensource hardware.

## drawbacks

New framebuffer dirvers should implement drm. If you where lazy like me and just wanted to check your ideas this will lead you to shiny xeyes on any monochrome framebuffer. Why do you want that? No X11 (xorg) driver supports the MONOCHROME color scheme. For good reasons. But if you have a MONOCHROME framebuffer driver for testing wouldn't it be nice to see xeyes and other stuff which do not rely on opengl, like gtk3+ on that? If your answer is yes check out this project. If no do the right thing and stay away!

## How does this thing works?

This implementation relies on xvfb which is a virtual framebuffer without any input drivers! Compile the hackish project and have a framebuffer on /dev/fb1 create a fbdev framebuffer with the right reolution and connect the two things together. Then just start xeyes on that framebuffer and see it on the real framebuffer. It uses the damage protocol to copy the pixels and transform them to monochrome to the framebuffer. if you want to have user interaction you will need to provide a connector to evdev and translate that to xevents.

## Bugs?
Tons of them - you have been warned with "do not use this for production code"

## Contribuitons?

Just perform a PR