#include <stdio.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <X11/extensions/Xdamage.h>
#include <X11/extensions/sync.h>

/*
 * Convert 16 bit per pixel to 1 bit per pixel
 */
static uint8_t get_byte(uint8_t *buffer)
{
	size_t i = 0;
	size_t y = 0;
	uint8_t bits[8]={0,0,0,0,0,0,0,0};

	for(i=0;i<16;i=i+2) {
		uint16_t val = buffer[i+1]*256 + buffer[i];
		uint8_t r5 = (val>>11) & 0x1F ;
		uint8_t g6 = (val>>5 ) & 0x3F ;
		uint8_t b5 = (val) & 0x1F ;
		bits[y] = (r5 > 0) | (g6 > 0) | (b5 > 0); // we can introduce different filters
		y++;
	}
	uint8_t byte = bits[7] | (bits[6] << 1 )| (bits[5] << 2) | (bits[4] << 3) | ( bits[3] << 4) | (bits[2] << 5) | (bits[1] << 6) | (bits[0] << 7);
	return byte;
}

static void write_image(XImage * image, int width, int height, int fd)
{
	uint8_t *dest = malloc((width * height)/8);

	size_t i=0;
	size_t j=0;

	size_t buffer_size= width * height * image->bits_per_pixel/8;
	printf("image->bits_per_pixel %i\n", image->bits_per_pixel);
	char* image_data = image->data;

	for (i=0;i<buffer_size;i=i+image->bits_per_pixel) {
		uint8_t buff[image->bits_per_pixel];
		memcpy(buff, image_data+i, image->bits_per_pixel);
		dest[j++] = get_byte(buff);
	}

	int ret = write(fd, dest, (width * height)/8);
        if (ret < 0) {
                printf("Failed to write to frame buffer device: %s\n",
                           strerror(errno));
        }
	lseek(fd, 0, SEEK_SET);
	free(dest);
}

int main(int argc, char *argv[])
{
    Display *display = XOpenDisplay(NULL);
    Window rootWindow = RootWindow(display, DefaultScreen(display));
	XImage *image;

	XWindowAttributes window_attributes_return;
	//FIMXE:check results
	Status ret = XGetWindowAttributes(display, rootWindow, &window_attributes_return);

	int damage_event, damage_error; // The event base is important here
	XDamageQueryExtension( display, &damage_event, &damage_error );
	//FIXME: check status

	// Create a damage handle for the window, and specify that we want an event whenever the
	// damage state changes from not damaged to damaged.
	Damage damage = XDamageCreate( display, rootWindow, XDamageReportNonEmpty );

	printf("window_attributes_return.witdh %i\n", window_attributes_return.width);
	printf("window_attributes_return.height %i\n", window_attributes_return.height);
	printf("window_attributes_return.depth %i\n", window_attributes_return.depth);
	int fd = open("/dev/fb1", O_CREAT|O_WRONLY,  S_IRUSR | S_IWUSR);
        if (fd < 0) {
                printf("Failed to open frame buffer device: %s\n",
                           strerror(errno));
        }

	image = XGetImage(display, rootWindow, 0, 0, window_attributes_return.width, window_attributes_return.height, AllPlanes, ZPixmap);
	write_image(image, window_attributes_return.width, window_attributes_return.height, fd);
	XFree(image);

	XEvent event;

	Time timestamp = 0;
	for (;;) {
		XNextEvent(display, &event);
		if ( event.type == damage_event + XDamageNotify ) {
			XDamageNotifyEvent *e = (XDamageNotifyEvent*)&event;
			XDamageSubtract( display, e->damage, None, None );
			printf("e->timestamp %lu, timestamp %lu", e->timestamp, timestamp);
			if(e->timestamp > timestamp)
			{
				image = XGetImage(display, rootWindow, 0, 0, window_attributes_return.width, window_attributes_return.height, AllPlanes, ZPixmap);
				write_image(image, window_attributes_return.width, window_attributes_return.height, fd);
				XFree(image);
				timestamp = e->timestamp;
			}
	}


	}
}